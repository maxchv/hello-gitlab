<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<body>
<h2>File Uploads Demo</h2>
<form method="post" action="/upload" enctype="multipart/form-data">
    <input name="file" type="file" multiple/>
    <button type="submit">Send</button>
</form>
<ul>
    <c:forEach items="${files}" var="file">
        <li><a target="_blank" title="Download ${file.getFileName()}" href="/resources/upload/${file.getFileName()}">${file.getFileName()}</a></li>
    </c:forEach>
</ul>
</body>
</html>
