package org.itstep.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;


@WebServlet("/upload")
@MultipartConfig()
public class UploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Path uploadDir = Paths.get(getServletContext().getRealPath("/"), "resources", "upload");
        for(Part p: req.getParts()) {
            if(p.getName().equals("file")) {
                String fileName = getFileName(p);
                if(fileName != null) {
                    Files.copy(p.getInputStream(), Paths.get(uploadDir.toAbsolutePath().toString(), fileName),
                            StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }

        resp.sendRedirect("/");
    }

    private String getFileName(Part part) {
        String fileName = null;

        String contentDisposition = part.getHeader("Content-Disposition");
        try {
            // FIXME: проблема с кириллическими именами файлов
            String[] data = URLDecoder.decode(contentDisposition, "UTF-8").split(";");
            for(String str: data) {
                String s = str.trim();
                if(s.startsWith("filename")) {
                    int idx = s.indexOf("=");
                    fileName = s.substring(idx+2, s.length() - 1);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return fileName;
    }
}
